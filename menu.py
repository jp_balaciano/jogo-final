from PPlay.sprite import *
from PPlay.sound import *

def menu(BotaoJogar, BotaoTutorial, BotaoFases, BotaoSair, mouse, janela, audio):
        #SOM DO LOBBY
        som_lobby = Sound("./jogo-final/Assets/Lobby.mp3")
        if audio:
            tocando_lobby = True
            som_icone = Sprite("./jogo-final/Assets/Som.png") 
        else:
            som_icone = Sprite("./jogo-final/Assets/Sem_som.png") 
            tocando_lobby = False

        #ICONE MUSICA
        som_icone.set_position(janela.width/2 + 380, janela.height/2 - 280)

        #VARIAVEIS
        resposta = 0
        cronometro_musica = 0
        cronometro_menu = 0

        
        while True: 
            #SOM
            if tocando_lobby:
                som_lobby.play()
                som_lobby.set_volume(2)

            #ICONE SOM
            if mouse.is_button_pressed(1) and (janela.width/2 + 380 - som_icone.width) <= mouse.get_position()[0] <= (janela.width/2 + 380 + som_icone.width) and cronometro_musica > 0.3:
            
                if janela.height/2 - 280 <= mouse.get_position()[1] <= janela.height/2 - 280 + som_icone.height:
                    if tocando_lobby == False:
                        som_icone = Sprite("./jogo-final/Assets/Som.png")
                        som_icone.set_position(janela.width/2 + 380, janela.height/2 - 280)
                        tocando_lobby = True
                        audio = True 
                        som_lobby.unpause()
                    else:
                        som_icone = Sprite("./jogo-final/Assets/Sem_som.png")
                        som_icone.set_position(janela.width/2 + 380, janela.height/2 - 280)
                        tocando_lobby = False
                        audio = False
                        som_lobby.pause()
                cronometro_musica = 0

            if mouse.is_button_pressed(1) and (janela.width - BotaoJogar.width)/2 <= mouse.get_position()[0] <= (janela.width + BotaoJogar.width)/2 and cronometro_menu > 1:
                if janela.height/2 - 120 <= mouse.get_position()[1] <= janela.height/2 - 120 + BotaoJogar.height:
                    resposta = 1
                    som_lobby.stop()
                    return resposta, audio
                elif janela.height/2 - 20 <= mouse.get_position()[1] <= janela.height/2 - 20 + BotaoTutorial.height:
                    resposta = 2
                    som_lobby.stop()
                    return resposta, audio
                elif janela.height/2 + 80 <= mouse.get_position()[1] <= janela.height/2 + 80 + BotaoFases.height:
                    resposta = 3
                    som_lobby.stop()
                    return resposta, audio
                elif  janela.height/2 + 180 <= mouse.get_position()[1] <=  janela.height/2 + 180 + BotaoSair.height:
                    resposta = 4
                    som_lobby.stop()
                    return resposta, audio
            
            cronometro_musica += janela.delta_time()
            cronometro_menu += janela.delta_time()

            BotaoJogar.draw()
            BotaoTutorial.draw()
            BotaoFases.draw()
            BotaoSair.draw()
            som_icone.draw()

            janela.update()







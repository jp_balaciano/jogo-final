from PPlay.sprite import *
from PPlay.sound import *

def fases(But_Fase1, But_Fase2, But_Fasefinal, mouse, janela, teclado, audio, niveis):
        #SOM DO LOBBY
        som_lobby = Sound("./jogo-final/Assets/Lobby.mp3")
        if audio:
            tocando_lobby = True
            som_icone = Sprite("./jogo-final/Assets/Som.png")
        else:
            tocando_lobby = False
            som_icone = Sprite("./jogo-final/Assets/Sem_som.png")

        #ICONE MUSICA 
        som_icone.set_position(janela.width/2 + 380, janela.height/2 - 280)

        #VARIAVEIS
        fase = 0
        cronometro_musica = 0
        cronometro_fase = 0

        while True: 
            janela.set_background_color([0, 0, 0])


            #SOM
            if tocando_lobby:
                som_lobby.play()
                som_lobby.set_volume(2)

            #ICONE SOM
            if mouse.is_button_pressed(1) and (janela.width/2 + 380 - som_icone.width) <= mouse.get_position()[0] <= (janela.width/2 + 380 + som_icone.width) and cronometro_musica > 0.3:
            
                if janela.height/2 - 280 <= mouse.get_position()[1] <= janela.height/2 - 280 + som_icone.height:
                    if tocando_lobby == False:
                        som_icone = Sprite("./jogo-final/Assets/Som.png")
                        som_icone.set_position(janela.width/2 + 380, janela.height/2 - 280)
                        tocando_lobby = True
                        audio = True
                        som_lobby.unpause()
                    else:
                        som_icone = Sprite("./jogo-final/Assets/Sem_som.png")
                        som_icone.set_position(janela.width/2 + 380, janela.height/2 - 280)
                        tocando_lobby = False
                        audio = False
                        som_lobby.pause()
                cronometro_musica = 0

            #SE TIVER 3 NIVEIS DESBLOQUEADOS:
            if niveis == 3:
                if mouse.is_button_pressed(1) and (janela.width - But_Fase1.width)/2 <= mouse.get_position()[0] <= (janela.width + But_Fase1.width)/2 and cronometro_fase > 1:
                    if janela.height/2 - 120 <= mouse.get_position()[1] <= janela.height/2 - 120 + But_Fase1.height:
                        fase = 1
                        som_lobby.stop()
                        return fase, audio
                    elif janela.height/2 - 20 <= mouse.get_position()[1] <= janela.height/2 - 20 + But_Fase2.height:
                        fase = 2
                        som_lobby.stop()
                        return fase, audio
                    elif janela.height/2 + 80 <= mouse.get_position()[1] <= janela.height/2 + 80 + But_Fasefinal.height:
                        fase = 3
                        som_lobby.stop()
                        return fase, audio                
                But_Fase1.draw()
                But_Fase2.draw()
                But_Fasefinal.draw()
                som_icone.draw()

            #SE TIVER 2 NIVEIS DESBLOQUEADOS
            elif niveis == 2:
                if mouse.is_button_pressed(1) and (janela.width - But_Fase1.width)/2 <= mouse.get_position()[0] <= (janela.width + But_Fase1.width)/2 and cronometro_fase > 1:
                    if janela.height/2 - 120 <= mouse.get_position()[1] <= janela.height/2 - 120 + But_Fase1.height:
                        fase = 1
                        som_lobby.stop()
                        return fase, audio
                    elif janela.height/2 - 20 <= mouse.get_position()[1] <= janela.height/2 - 20 + But_Fase2.height:
                        fase = 2
                        som_lobby.stop()
                        return fase, audio
                But_Fase1.draw()
                But_Fase2.draw()
                som_icone.draw()

            #SE TIVER 1 NIVEL DESBLOQUEADO
            elif niveis == 1:
                if mouse.is_button_pressed(1) and (janela.width - But_Fase1.width)/2 <= mouse.get_position()[0] <= (janela.width + But_Fase1.width)/2 and cronometro_fase > 1:
                    if janela.height/2 - 120 <= mouse.get_position()[1] <= janela.height/2 - 120 + But_Fase1.height:
                        fase = 1
                        som_lobby.stop()
                        return fase, audio
                But_Fase1.draw()
                som_icone.draw()
        
    
            
            cronometro_fase += janela.delta_time()
            cronometro_musica += janela.delta_time()


            if teclado.key_pressed("ESC"):
                return fase, audio

            janela.update()







from PPlay.window import *
from PPlay.keyboard import *
from PPlay.animation import *
from PPlay.sprite import *
from PPlay.gameimage import *
from jogar import *
from menu import *
from tutorial import *
from fases import *
from completou import *

#JANELA 
janela = Window(1000, 600)
janela.set_title("Bud Journey")
janela.set_background_color([0, 0, 0])


#TECLADO E MOUSE
teclado = Window.get_keyboard()
mouse = Window.get_mouse()


#Botões menu
BotaoJogar = Sprite("./jogo-final/Assets/But_Jogar.png", frames=1)
BotaoTutorial = Sprite("./jogo-final/Assets/But_Tutorial.png", frames=1)
BotaoFases = Sprite("./jogo-final/Assets/But_Fases.png", frames=1)
BotaoSair = Sprite('./jogo-final/Assets/But_Sair.png', frames=1)


#Posição Botão
BotaoJogar.set_position(janela.width/2 - BotaoJogar.width/2, janela.height/2 - 120)
BotaoFases.set_position(janela.width/2 - BotaoTutorial.width/2, janela.height/2 - 20)
BotaoTutorial.set_position(janela.width/2 - BotaoFases.width/2, janela.height/2 + 80)
BotaoSair.set_position(janela.width/2 - BotaoSair.width/2, janela.height/2 + 180)


#Botões Fases
But_Fase1 = Sprite("./jogo-final/Assets/But_Fase1.png", frames=1)
But_Fase2 = Sprite("./jogo-final/Assets/But_Fase2.png", frames=1)
But_Fasefinal = Sprite("./jogo-final/Assets/But_Fasefinal.png", frames=1)


#Posição Botão Fases
But_Fase1.set_position(janela.width/2 - BotaoJogar.width/2, janela.height/2 - 120)
But_Fase2.set_position(janela.width/2 - BotaoTutorial.width/2, janela.height/2 - 20)
But_Fasefinal.set_position(janela.width/2 - BotaoFases.width/2, janela.height/2 + 80)

#SPRITES PERSONAGENS
bud = Sprite("./jogo-final/Assets/dragao.png", 8)
bud.set_sequence_time(0, 7, 60, True)
bud.set_position(janela.width/2 - bud.width/2, janela.height/2 - bud.height/2 )
vel_bud = 500

audio = True
rodar = True
fase = 1
niveis = 1
ganhou = False



while rodar:
    janela.set_background_color([0, 0, 0])

    #TITULO
    janela.draw_text("BUDJOURNEY",janela.width/2 - 190,janela.height/2-250,75,(255,255,255),"Impact",False,False)

    resposta, audio = menu(BotaoJogar, BotaoTutorial, BotaoFases, BotaoSair, mouse, janela, audio)


    #-----------------------------------------------------------------------------------------------------------------
    #BOTOES DO MENU 
    if resposta == 1:
        if fase == 0:
            fase = 1
        fase,ganhou,audio = jogar(bud, teclado, mouse, janela, fase, audio)
        if ganhou:
            while ganhou:
                if fase != 3:
                    fase += 1
                    ganhou = False
                    fase,ganhou,audio = jogar(bud, teclado, mouse, janela, fase, audio)
                    if ganhou == False:
                        niveis = fase
                        fase = 1
                else:
                    niveis = fase
                    fase = 1
                    break

    elif resposta == 2:
        fase, audio = fases(But_Fase1, But_Fase2, But_Fasefinal, mouse, janela, teclado, audio, niveis)
        if fase != 0:
            fase, ganhou,audio = jogar(bud, teclado, mouse, janela, fase, audio)
            if ganhou:
                while ganhou:
                    if fase != 3:
                        fase += 1
                        ganhou = False
                        fase,ganhou,audio = jogar(bud, teclado, mouse, janela, fase, audio)
                        if ganhou == False:
                            niveis = fase
                            fase = 1
                    else:
                        niveis = fase
                        fase = 1
                        break
        

    elif resposta == 3:
        tut = True
        while tut:
            tutorial(janela)
            if teclado.key_pressed("ESC"):
                tut = False

    elif resposta == 4:
        janela.close()
    #-----------------------------------------------------------------------------------------------------------------------

   

    janela.update()
 
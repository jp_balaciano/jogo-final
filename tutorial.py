from PPlay.window import *


def tutorial(janela):
    janela.set_background_color([0, 0, 0])

    janela.draw_text("TUTORIAL",janela.width/2-460,janela.height/2-300,75,(255,255,255),"Arial",False,False)
    janela.draw_text("Objetivo do jogo: Sobreviva durante 60 segundos,",janela.width/2-460,janela.height/2-200,30,(255,255,255),"Arial",False,False)
    janela.draw_text("desviando dos obstáculos que irão aparecer",janela.width/2-460,janela.height/2-150,30,(255,255,255),"Arial",False,False)
    janela.draw_text("<= (A): Mover o player para a esquerda",janela.width/2-460,janela.height/2-50,30,(255,255,255),"Arial",False,False)
    janela.draw_text("=> (D): Mover o player para a direita",janela.width/2-460,janela.height/2,30,(255,255,255),"Arial",False,False)
    janela.draw_text("SPACE: Pulo do player",janela.width/2-460,janela.height/2+100,30,(255,255,255),"Arial",False,False)
    
    janela.update()

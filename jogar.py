from PPlay.sprite import *
from PPlay.collision import *
from random import randint
from PPlay.sound import *
from completou import *

def jogar(bud, teclado, mouse, janela, fase, audio):
    #VARIAVEIS 
    velocidade = 5
    cronometro = 0
    fps = 60
    gravidade = 0
    play = False
    game_over = False
    ganhou = False
    c_D = 0

    if fase == 1:
        fundo = 1
        v_espada = 7
        cd = 1.5
    elif fase == 2:
        fundo = 2
        v_espada = 10
        cd = 1
    elif fase == 3:
        fundo = 3
        v_espada = 13
        cd = 0.5

    cronometro_tot = 0

    l_x=[100,200,300,400,500,600,700,800]
    l_y=[200, 300, 400]

    l_espadas1=[]
    l_espadas2=[]
    l_espadas3=[]
    l_espadas4=[]

    #SPRITES FUNDO
    fundo1 = Sprite("./jogo-final/Assets/fundo_fase1.png")
    fundo2 = Sprite("./jogo-final/Assets/fundo_fase2.png")
    fundo3 = Sprite("./jogo-final/Assets/fundo_fase3.png")
    fundo1.set_position(janela.width/2 - fundo1.width/2, janela.height/2 - fundo1.height/2)
    fundo2.set_position(janela.width/2 - fundo2.width/2, janela.height/2 - fundo2.height/2)
    fundo3.set_position(janela.width/2 - fundo3.width/2, janela.height/2 - fundo3.height/2)

    bud.set_position(janela.width/2 - bud.width/2, janela.height/2 - bud.height/2 )

    #Botão de musica
    som_icone = Sprite("./jogo-final/Assets/Som.png") 
    som_icone.set_position(janela.width/2 + 380, janela.height/2 - 280)

    #Musica
    som = Sound("./jogo-final/Assets/musica.mp3")
    som_over = Sound("./jogo-final/Assets/gameover.mp3")
    if audio:
        tocando = True
        tocando_over = False
        cronometro_musica = 0
        som_icone = Sprite("./jogo-final/Assets/Som.png")
    else:
        tocando = False
        tocando_over = False
        cronometro_musica = 0
        som_icone = Sprite("./jogo-final/Assets/Sem_som.png")

    som_icone.set_position(janela.width/2 + 380, janela.height/2 - 280)

    

    while True:
        if teclado.key_pressed("SPACE"):
            if game_over == False:
                play = True
                if tocando == True:
                    som.play()
                    som.set_volume(2)

        #Muda o Icone do audio
        if mouse.is_button_pressed(1) and (janela.width/2 + 380 - som_icone.width) <= mouse.get_position()[0] <= (janela.width/2 + 380 + som_icone.width) and cronometro_musica > 0.3:
            if janela.height/2 - 280 <= mouse.get_position()[1] <= janela.height/2 - 280 + som_icone.height:
                if tocando == False:
                    som_icone = Sprite("./jogo-final/Assets/Som.png")
                    som_icone.set_position(janela.width/2 + 380, janela.height/2 - 280)
                    tocando = True
                    audio = True
                    som.unpause()
                else:
                    som_icone = Sprite("./jogo-final/Assets/Sem_som.png")
                    som_icone.set_position(janela.width/2 + 380, janela.height/2 - 280)
                    tocando = False
                    audio = False
                    som.pause()
            cronometro_musica = 0
        
        
        if play:
            #movimento
            if (teclado.key_pressed("RIGHT") or teclado.key_pressed("D")) and (bud.x < janela.width - bud.width):
                bud.x += velocidade * (fps * janela.delta_time())   
            if (teclado.key_pressed("LEFT") or teclado.key_pressed("A")) and (bud.x > 0):
                bud.x -= velocidade * (fps * janela.delta_time())
            
            #gravidade
            if gravidade < 5:
                gravidade += 0.2

            if bud.y < janela.height - bud.height:
                bud.y += gravidade * (fps * janela.delta_time())
            elif bud.y > janela.height - bud.height:
                bud.set_position(bud.x, bud.y - 5)
                bud.y += gravidade * (fps * janela.delta_time())
            
            #pular
            if teclado.key_pressed("SPACE") and (bud.y > 0):
                gravidade = -5
            
            #obstaculos
            lado = randint(1,2) #1-em cima, 2-embaixo
            if lado == 1:
                if cronometro > cd:
                    espada = Sprite("./jogo-final/Assets/Flecha_baixo.png")
                    espada.set_position(l_x[randint(0, len(l_x)-1)], 0-espada.height)
                    l_espadas1.append(espada)
                    cronometro = 0
                
                for e in l_espadas1: 
                    e.y += v_espada * (fps * janela.delta_time())
                
                for e in range(len(l_espadas1) - 1,-1,-1):
                    if l_espadas1[e].y == janela.height:
                        l_espadas1.pop(e)
            
            if lado == 2:
                if cronometro > cd:
                    espada = Sprite("./jogo-final/Assets/Flecha_cima.png")
                    espada.set_position(l_x[randint(0, len(l_x)-1)], janela.height)
                    l_espadas2.append(espada)
                    cronometro = 0
                
                for e in l_espadas2: 
                    e.y -= v_espada * (fps * janela.delta_time())
                
                for e in range(len(l_espadas2) - 1,-1,-1):
                    if l_espadas2[e].y <= -espada.height:
                        l_espadas2.pop(e)

            cronometro_tot += janela.delta_time()
            
        #Colisão
        for f in l_espadas1:
            if Collision.collided(f, bud):
                    l_espadas1.remove(f)
                    game_over = True
                    play = False
        for f in l_espadas2:
            if Collision.collided(f, bud):
                    l_espadas2.remove(f)
                    game_over = True
                    play = False

        #Desenha fundo (mudar depois tb)
        if fundo == 1:
            fundo1.draw()
        elif fundo == 2:
            fundo2.draw()
        elif fundo == 3:
            fundo3.draw()
        
        #Fechar jogo
        if teclado.key_pressed("ESC"):
            bud.set_position(janela.width/2 - bud.width/2, janela.height/2 - bud.height/2 )
            cronometro_tot = 0
            som.stop()
            return fase, ganhou, audio

        #Atualiza cronometro
        cronometro += janela.delta_time()
        cronometro_musica += janela.delta_time()

        #desenha flechas
        if play:
            for e in l_espadas1:
                e.draw()
            for e in l_espadas2:
                e.draw()
            for e in l_espadas3:
                e.draw()
            for e in l_espadas4:
                e.draw()
        
        #Desenha bud e som
        bud.draw()
        bud.update()
        som_icone.draw()

        #Cronometro
        if game_over == False:
            if cronometro_tot >= 5 and cronometro_tot < 10:
                if c_D < 0.5:
                    janela.draw_text(str(int(cronometro_tot)),janela.width/2 - 380, janela.height/2 - 280,75,(255,0,0),"Arial",True,False)
                elif c_D > 1:
                    c_D = 0
                c_D += janela.delta_time()
            elif cronometro_tot < 10:
                janela.draw_text(str(int(cronometro_tot)),janela.width/2 - 380, janela.height/2 - 280,75,(0,0,255),"Arial",True,False)
            elif cronometro_tot >= 10:
                if fase != 3:
                    ganhou = True
                    janela.draw_text("VOCÊ GANHOU",janela.width/2-250,janela.height/2-200,75,(0,0,0),"Arial",True,False)
                    janela.draw_text("Clique M para mudar de fase",janela.width/2-300,janela.height/2-100,50,(0,0,0),"Arial",True,False)
                    janela.draw_text("Clique ESC para sair",janela.width/2-285,janela.height/2-25,50,(0,0,0),"Arial",True,False)
                    l_espadas1=[]
                    l_espadas2=[]
                    som.stop()
                    play = False
                    gravidade = 0
                    if teclado.key_pressed("M"):
                        return fase, ganhou, audio
                    
                else:
                    ganhou = True
                    l_espadas1=[]
                    l_espadas2=[]
                    som.stop()
                    play = False
                    gravidade = 0
                    play = False
                    completou(janela)



        if play == False:
            if game_over == True:
                janela.draw_text("GAME OVER",janela.width/2-200,janela.height/2-200,75,(0,0,0),"Arial",True,False)
                janela.draw_text("Clique M para reniciar o jogo",janela.width/2-300,janela.height/2-100,50,(0,0,0),"Arial",True,False)
                janela.draw_text("Clique ESC para sair",janela.width/2-285,janela.height/2-25,50,(0,0,0),"Arial",True,False)
                l_espadas1=[]
                l_espadas2=[]
                som.stop()
                if tocando_over == False:
                    som_over.play()
                    tocando_over = True

                if teclado.key_pressed("M"):
                    play = True
                    game_over = False
                    tocando_over = False
                    cronometro_tot = 0
                    bud.set_position(janela.width/2 - bud.width/2, janela.height/2 - bud.height/2 )

                         
        janela.update()
